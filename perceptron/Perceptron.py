import numpy as np

class Perceptron():
	def __init__(self, input_dim, init=None):
		if init == 'random':
			self.weights = np.random.rand(input_dim)
			self.bias = np.random.rand(1)
		else:
			self.weights = np.zeros(input_dim, dtype=np.float64)
			self.bias = .0

	def update(self, delta_weights, delta_bias):
		self.weights += delta_weights
		self.bias += delta_bias

	def train(self, inputs, targets, max_it=0):
		corect_classify = 0
		it = -1
		iterazioni = 0
		while corect_classify < inputs.shape[0] and it < max_it:
			for (x, y) in zip(inputs, targets):
				iterazioni += 1
				if y * (np.dot(self.weights, x) + self.bias) <= 0:
					self.update(y * x, y)
				else:
					corect_classify += 1
			if corect_classify < inputs.shape[0]:
				corect_classify = 0
			if max_it != 0:
				it += 1
		print("iterazioni: "+str(iterazioni))

	def predict(self, input):
		return np.dot(self.weights, input) + self.bias

	def get_weights(self):
		return self.weights
	def get_bias(self):
		return self.bias


class VotingPerceptron(Perceptron):
	def __init__(self, input_dim, init=None):
		super().__init__(input_dim, init)
		if init == 'random':
			self.better_weights = np.random.rand(input_dim, dtype=np.float64)
			self.better_bias = np.random.rand(1, dtype=np.float64)
		else:
			self.better_weights = np.zeros(input_dim, dtype=np.float64)
			self.better_bias = .0 

	def train(self, inputs, targets, max_it):
		it = 0
		iterazioni = 0
		surv = 0
		surv_max = 0
		while it < max_it:
			for (x, y) in zip(inputs, targets):
				iterazioni += 1
				if y * (np.dot(self.weights, x) + self.bias) <= 0:
					self.update(y * x, y)
					if surv > surv_max:
						print("#######aggiornamento del best#######")
						print("voti ottenuti: "+ str(surv))
						self.better_weights = self.weights
						self.better_bias = self.bias
						surv_max = surv
						surv = 0
				else:
					surv += 1
			it += 1
		print("iterazioni: "+str(iterazioni))

	def predict(self, input):
		return np.dot(self.better_weights, input) + self.better_bias

	def get_weights(self):
		return self.better_weights
	def get_bias(self):
		return self.better_bias

class AveragePerceptron(Perceptron):
	def __init__(self, input_dim, init=None):
		super().__init__(input_dim, init)
		if init == 'random':
			self.avg_weights = np.random.rand(input_dim, dtype=np.float64)
			self.avg_bias = np.random.rand(1, dtype=np.float64)
		else:
			self.avg_weights = np.zeros(input_dim, dtype=np.float64)
			self.avg_bias = .0

	def train(self, inputs, targets, max_it):
		it = 0
		voto = 0
		update = 0
		while it < max_it:
			for (x, y) in zip(inputs, targets):
				if y * (np.dot(self.weights, x) + self.bias) <= 0:
					self.update(y * x, y)
					#update += 1
					self.avg_weights += voto * self.weights #/ update
					self.avg_bias += voto * self.bias #/ update
				else:
					voto += 1
			it += 1

	def predict(self, input):
		return np.dot(self.avg_weights, input) + self.avg_bias

	def get_weights(self):
		return self.avg_weights
	def get_bias(self):
		return self.avg_bias

'''
in develop
class NonLinearPerceptron(Perceptron):

	def __init__(self, input_dim, inputs, targets, init=None):
		super().__init__(input_dim, init)
		self.inputs = inputs
		self.targets = targets

	def forward(self, input):
		return np.tanh(np.dot(self.weights, input) + self.bias)
	
	def errorFun(self, inputs):
		error = 0
		for (x,y) in zip(inputs, self.targets):
			error += (y - self.forward(x))**2
		error = error / inputs.shape[0]
		return error

	def get_weights(self):
		return self.weights
	def get_bias(self):
		return self.bias
'''