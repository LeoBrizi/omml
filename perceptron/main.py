from Perceptron import *
import numpy as np 
import matplotlib.pyplot as plt
from scipy import optimize


c1 = np.array(list(zip(np.random.normal(10, 5, 20), np.random.normal(2, 5, 20))))
t1 = np.negative(np.ones(20))

c2 = np.array(list(zip(np.random.normal(0, 5, 30), np.random.normal(10, 5, 30))))
t2 = np.ones(30)

inputs = np.concatenate((c1, c2), axis=0)
target = np.concatenate((t1, t2))

order = np.arange(inputs.shape[0])
np.random.shuffle(order)
inputs = inputs[order]
target = target[order]

perc = AveragePerceptron(2)

perc.train(inputs, target, 5000)

X = [-8, 15]
def retta(x):
	return -perc.get_weights()[0]/perc.get_weights()[1]*x - perc.get_bias()/perc.get_weights()[1]

Y = [ retta(X[0]), retta(X[1])]
fig = plt.figure()
plt.plot(c1[:,0], c1[:,1], 'x')
plt.plot(c2[:,0], c2[:,1], 'o')
plt.plot(X, Y, '-')
plt.show()

'''
per = NonLinearPerceptron(2, inputs, target, 'random')

print("prova "+str(per.errorFun(inputs)))

optimize.minimize(per.errorFun, inputs, method='Nelder-Mead')
'''