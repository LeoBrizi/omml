import numpy as np
from cvxopt import matrix, solvers
import time


class SVM(object):

	def __init__(self, C, gamma, kernel='RBF', treshold=1e-7, verbose=True):
		'''
		constructor of the model
		C: penalty of misclassification
		gamma: parametre of the kernel function
		kernel: kind of kernel
		threshold: number to consider equal to zero
		verbose: boolean variable to have prints during computation 
		'''
		self.C = C
		self.gamma = gamma
		self.kernel = kernel
		self.verbose = verbose
		self.treshold = treshold

	@staticmethod
	def __encode_label(Y):
		'''
		function to encode the labels always in the same way
		it returns the label encoded and the classes founded
		it raise an Exception if the label vector cointain more than 2 class
		this because SVM model can separate only two different sets of points 
		'''
		classes = np.unique(Y)
		if(classes.shape[0] != 2):
			raise Exception("Only two different types of classes is allowed")
		Y[Y == classes[0]] = -1
		Y[Y == classes[1]] = 1
		return Y, classes

	@staticmethod
	def __select_support_vectors(X, Y, lam, treshold):
		'''
		at the end of the training only the support vectors (lambda != 0) contribute at the decision function
		so at the end of the training we reduce the number of vectors to speed up the computation of the decision function
		it returns:
		sv_X: only the X associated to lambda != 0
		sv_Y:	only the Y associated to lambda != 0
		sv_lam: only the lambda != 0
		num_sv: number of support vector founded
		'''
		lam = np.squeeze(lam)
		sv_X = X[lam>treshold,:]
		sv_Y = Y[lam>treshold]
		sv_lam = lam[lam>treshold]
		num_sv = sv_lam.shape[0]
		return sv_X, sv_Y.reshape(sv_Y.shape[0], 1), sv_lam.reshape(sv_lam.shape[0], 1), num_sv

	@staticmethod
	def __find_best_b(lam, C, P, Y, X, treshold, kernel, gamma):
		'''
		function to find the B star
		we have to find a 0 < lambda < C and compute the B star with the formula 5.29
		it returns the B star
		'''
		best_b = 0
		for i in range(P):
			if(lam[i] > treshold and lam[i] < C - treshold):
				#if we enter here we have found a lambda between 0 and C
				sm = 0
				#compute the summation: sum_{i:1,P}(lamda^j*y^i*Kernel(x^j,x^i))
				if(kernel == 'RBF'):
					for j in range(P):
						sm += lam[j] * Y[j] * SVM.__RBF_kernel(X[j] - X[i], gamma)
				else:
					for j in range(P):
						sm += lam[j] * Y[j] * SVM.__polynomial_kernel(np.dot(X[i], X[j]), gamma)
				best_b = Y[i] - sm
				break
		return best_b

	@staticmethod
	def __RBF_kernel(t, gamma):
		'''
		function for the gaussian kernel
		it takes as input the x-y
		and it returns e^(-gamma * ||x-y||^2)
		'''
		return np.exp( -gamma * t.dot(t))

	@staticmethod
	def __polynomial_kernel(t, gamma):
		'''
		function for the polynomial kernel
		it takes as input the xy
		and it returns (xy + 1)^gamma
		'''
		return (t + 1)**gamma

	@staticmethod
	def __compute_Q(P, Y, X, kernel, gamma):
		'''
		it computes the matrix of elements y[i]*y[j]*k(x[i],x[j])
		the quadratic part of the optimization problem
		it takes as input the training set data points the kernel to use and the gamma
		it returns the Q matrix of the dual objective function  
		'''
		#initialize the matrix
		Q = np.zeros((P, P))
		if(kernel == 'RBF'):
			#fill the matrix only above the principal diagonal, because it is symmetric
			for i in range(P):
				for j in range(i, P): 
					Q[i, j] = Y[i] * Y[j] * SVM.__RBF_kernel(X[i] - X[j], gamma)
			#fill the remaining part below the principal digonal
			Q = Q + Q.T - np.diag(np.diag(Q))
		else:
			for i in range(P):
				for j in range(i, P): 
					Q[i, j] = Y[i] * Y[j] * SVM.__polynomial_kernel(np.dot(X[i], X[j]), gamma)
			Q = Q + Q.T - np.diag(np.diag(Q))
		return Q

	@staticmethod
	def __compute_m_M(grad, Y, R, S):
		'''
		this function computes the m(alpha) and M(alpha)
		they are needed to comput the kkt condition
		which is m(alpha)-M(alpha)
		m(alpha) = max_{i in R}((-grad)[i]/Y[i])
		M(alpha) = min_{i in S}((-grad)[i]/Y[i])
		it takes in input the gradient of the objective function, Y and the two sets of indeces R, S
		it returns m(alpha) and M(alpha)
		'''
		temp = -1 * grad / Y
		m = np.max(temp[R])
		M = np.min(temp[S])
		return m, M

	@staticmethod
	def __divide_set(lam, Y, C, treshold):
		'''
		this function compute the sets L+, L-, U+, U-, F 
		they are needed to compute the two sets R and S
		it takes as input lambda, the labels Y, C and the threshold to consider a number zero
		and it returns the sets L+, L-, U+, U-, F
		'''
		#takes the indeces of lambda where the lambda[i] == 0
		L = np.where(abs(lam) <= treshold)[0]
		#takes the indeces of lambda where the lambda[i] == C
		U = np.where(abs(lam - C) <= treshold)[0]
		#divide the indeces according the sign of Y
		Lp = [i for i in L if Y[i] > 0]
		Lm = [i for i in L if Y[i] < 0]
		Up = [i for i in U if Y[i] > 0]
		Um = [i for i in U if Y[i] < 0]
		#select the indeces of the free variables: variables that don't reach neither the lower bound and the upper bound
		F = np.array(range(0, Y.shape[0]))
		F = np.delete(F, np.append(L,U))
		return np.array(Lp, dtype=np.int), np.array(Lm, dtype=np.int), np.array(Up, dtype=np.int), np.array(Um, dtype=np.int), np.array(F, dtype=np.int)

	@staticmethod
	def __select_working_set(R, S, grad, Y, q):
		'''
		this function computes the working set of the decomposition
		it takes the indices oif the most violating lambda 
		to make this we order the set of the indices R in descendent order
		respect to  -1 * grad[R] / Y[R] and order the set S in crescent order
		respect to  -1 * grad[S] / Y[S] then select q/2 from the top of R and
		q/2 from the top of S
		it takes as input the sets R,S and the gradient of the objective function
		in the current point and the Y labels and how many indices to take q
		it return the working set
		'''
		criterio_R = -1 * grad[R] / Y[R]
		predicate_R = -1 * criterio_R.flatten()
		order = np.argsort(predicate_R)
		R_ord = R[order]
		W = R_ord[0:q//2]
		
		S_temp = np.delete(S, W)

		criterio_S = -1 * grad[S_temp] / Y[S_temp]
		predicate_S = criterio_S.flatten()
		order = np.argsort(predicate_S)
		S_ord = S_temp[order]
		W = np.append(W, S_ord[0:q//2])
		return W

	def train_decomposition(self, X, Y, q, max_it=10000, stop_crit=1e-3 ):
		'''
		function that has the aim to optimize the objective function of the dual problem
		it takes as input the dataset X and the labels Y
		and it found the optimal values of lambdas, the support vectors and the b* 
		this function implements the algorithm of SVMlight
		this algorith doesn't optimize the full problem but optimize the smal subproblem
		of the original
		at each iteration takes a working set and optimize respect to the lambdas 
		that belong to this set
		'''
		res = {}#dictory for the summary of the training
		solvers.options['abstol'] = 1e-12
		solvers.options['reltol'] = 1e-12 
		solvers.options['feastol'] = 1e-12
		P = X.shape[0]
		Y, self.classes = SVM.__encode_label(Y)
		Y = Y.reshape(P, 1)
		lam = np.zeros((P, 1)) #initial value of lambda
		grad = -1 * np.ones((P, 1)) #initial value of the gradient
		k = 0
		e_w = np.ones((q, 1))
		#kkt condition at the starting point
		m_lam = 1
		M_lam = -1
		solvers.options["show_progress"] = False
		if self.verbose:
			solvers.options["show_progress"] = True
		Q = SVM.__compute_Q(P, Y, X, self.kernel, self.gamma)
		start_opt = time.time()
		while(k < max_it and (m_lam - M_lam > stop_crit )):
			if self.verbose:
				print("iteration: ", k)
			start = time.time()
			#compute the R and S sets
			Lp, Lm, Up, Um, F = SVM.__divide_set(lam, Y, self.C, self.treshold)
			R = np.append(Lp, np.append(Um, F))
			S = np.append(Lm, np.append(Up, F))
			#select the working set
			working_set = SVM.__select_working_set(R, S, grad, Y,  q)
			#select the non_working set
			not_working_set = np.array(range(0, P))
			not_working_set = np.delete(not_working_set, working_set)
			#select the lambda of the working set
			lam_w = lam[working_set]
			#compute the matrices of the coeffiecient of the subproblem 5.52 on teaching notes
			Q_w_w = Q[working_set, :]
			Q_w_w = Q_w_w[:, working_set]

			Q_wb_w = Q[not_working_set, :]
			Q_wb_w = Q_wb_w[:, working_set]
			lam_wb = lam[not_working_set]

			p = np.dot(lam_wb.T, Q_wb_w) - e_w.T
			A = Y[working_set].T
			b = -1 * np.dot(Y[not_working_set].T, lam[not_working_set])
			G = np.append(-1 * np.eye(q), np.eye(q), axis=0)
			h = np.append(np.zeros(q), self.C * e_w).reshape(-1, 1)
			#transform the matrix in compatible way for cvxopt
			Q_cvxopt = matrix(Q_w_w)
			p_cvxopt = matrix(p.flatten())
			A_cvxopt = matrix(A)
			b_cvxopt = matrix(b.flatten())
			G_cvxopt = matrix(G)
			h_cvxopt = matrix(h.flatten())
			#run the optimizer
			sol = solvers.qp(Q_cvxopt, p_cvxopt, G_cvxopt, h_cvxopt, A_cvxopt, b_cvxopt)
			if self.verbose:
				print(sol)
			lam_old = lam.copy()
			#update the value of lambda
			lam[working_set] = np.array(sol["x"])
			#update gradient
			delta = lam[working_set] - lam_old[working_set]
			grad = grad + np.dot(Q[:,working_set], delta)
			#compute the kkt
			m_lam, M_lam = SVM.__compute_m_M(grad, Y, R, S)
			k += 1
			running_time = time.time() - start
			if(self.verbose):
				print("kkt: ", m_lam - M_lam)
				print("tempo di una iterazione: ", running_time)

		opt_time = time.time() - start_opt
		lam_init = np.zeros((P,1))
		res["opt_dual"] = 1/2 * np.dot(np.dot(lam.T, Q), lam) - np.sum(lam)	
		res["init_dual"] = 1/2 * np.dot(np.dot(lam_init.T, Q), lam_init) - np.sum(lam_init)
		#compute the support vectors and the b*
		self.X, self.Y, self.lam, self.num_sv = SVM.__select_support_vectors(X, Y, lam, self.treshold)
		self.b = SVM.__find_best_b(lam, self.C, P, Y, X, self.treshold, self.kernel, self.gamma)
		res["opt_time"] = opt_time
		res["opt_it"] = k +1
		res["kkt"] = m_lam - M_lam

		if self.verbose:
			print("train finished")
			print("num of support vectors: ", self.num_sv)
			print(res)
		
		return res

	def predict(self, x):
		'''
		this function implements the decision function of the svm
		it takes as input one single value and return the predicted label
		'''
		#K_x_x is the sum of the vectors K(x,support_vector)
		K_x_x = np.zeros(self.num_sv)
		if(self.kernel == 'RBF'):
			for i in range(self.num_sv):
				K_x_x[i] = SVM.__RBF_kernel(self.X[i] - x, self.gamma)
		else:
			for i in range(self.num_sv):
				K_x_x[i] = SVM.__polynomial_kernel(np.dot(self.X[i], x), self.gamma)
		return np.sign(K_x_x.dot(self.lam * self.Y) + self.b)

	def evaluate_accuracy(self, X, label):
		'''
		this function takes as input some data with the relative labels and evaluate the accuracy
		it is needed to train the hyperparameters
		because the library has as fixed interface for the models
		it returns the accuracy
		'''
		Y = np.copy(label)
		Y[Y == self.classes[0]] = -1
		Y[Y == self.classes[1]] = 1
		miss_class = 0
		for (x,y) in zip(X, Y):
			if (self.predict(x) != y):
				miss_class += 1
		return (Y.shape[0] - miss_class) / Y.shape[0]

	def predict_all(self, X):
		'''
		this function is the extension of the preticted function to vector of data
		it returns the labels predicted for each element of X
		'''
		Y_pred = np.array([])
		for x in X:
			y_pred = self.predict(x)
			Y_pred = np.append(Y_pred, y_pred)
		return Y_pred, self.classes

	def set_verbose(self, verbose):
		self.verbose = verbose
