from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt
import numpy as np 

'''
this library is designed to work with model which implemets the following functions:
-evaluate_accuracy: to compute the accuracy of the model on a dataset
-train: train the model with a dataset
-set_Hyper_parmas: change the hyper parameters of the model
'''

def divide_data(X, Y, times):
	#function to divide the dataset in small set of (data.shape[0]/times) elements 
	
	step = int(X.shape[0]/times)
	X_pools = np.zeros((times, step, X.shape[1]))
	Y_pools = np.zeros((times, step, Y.shape[1]))
	for i in range(times):
		X_pools[i] = X[(i*step):(i*step)+step , :]
		Y_pools[i] = Y[(i*step):(i*step)+step , :]
	return X_pools, Y_pools 

def K_cross_val_score(model, X, Y, cv):
	#k-cross validation for evaluation of hyperparameters
	#it returns the average of the training error and the average of the validation erros
	Y = Y.reshape(Y.shape[0], 1)
	validation_accu = np.zeros(cv)
	training_accu = np.zeros(cv)
	#get the sets from the original dataset 
	X_pools, Y_pools = divide_data(X, Y, cv)
	for i in range(cv):
		#use this set for evaluate the hyperparameters
		X_validation_set = X_pools[i]
		Y_validation_set = Y_pools[i]
		#use this set for training the parameters
		X_training_set = np.delete(X_pools, i, axis=0)
		X_training_set = X_training_set.reshape(X_training_set.shape[0]*X_training_set.shape[1], X.shape[1])
		Y_training_set = np.delete(Y_pools, i, axis=0)
		Y_training_set = Y_training_set.reshape(Y_training_set.shape[0]*Y_training_set.shape[1], Y.shape[1])
		#train the model
		try:
			model.train(X_training_set, Y_training_set)
		except ZeroDivisionError:
			continue
		#evaluate the model on train and validation
		validation_accu[i] = model.evaluate_accuracy(X_validation_set, Y_validation_set)
		training_accu[i] = model.evaluate_accuracy(X_training_set, Y_training_set)
	avg_validation_accu = np.average(validation_accu)
	avg_training_accu = np.average(training_accu)
	return avg_training_accu, avg_validation_accu


def grid_search(model, X, Y):
	#it make a grid_search on the possibile values for the hyperparameters
	#the function returns a dictionary where the key is the hyperparametes
	#and the value is the values of the errors (average_train, average_validation, test)
	result = {}
	#range for gamma 0.01 to 0.5 with step 0.01
	for gamma in range(1, 50, 1):
		gamma = gamma/100
		#range for C 1 to 5 with step 1
		for C in range(1, 5, 1):
			print("HYPER PARAMETERS: gamma: ", gamma, " C: ", C)
			#set the new hyper parameters of the model
			model.set_hyper_params(gamma=gamma, C=C)
			#run k-cross validation for these hyper param
			avg_train_accu, avg_val_accu = K_cross_val_score(model, X, Y, cv=3)
			result[str((gamma, C))] = (float(avg_train_accu), float(avg_val_accu))
			print("ACCURACY VALUES: avg_train_accu: ", avg_train_accu, " avg_val_accu: ", avg_val_accu)
	return result
	
def plot_heat_map(X, Y, C):
	#plot a 3 dimensional function with a heat map
	#this needed for better understand how choose better hyper parameters
	fig = plt.figure()
	# ax = fig.add_subplot(111, projection='3d')
	# img = ax.scatter(X, Y, c=C, cmap=plt.hot())
	# fig.colorbar(img)
	# plt.show()
	sizes = 50*np.ones(C.shape)
	plt.scatter(X, Y, c=C, s=sizes, alpha=0.3, cmap='viridis')
	plt.colorbar()
	plt.show()

def plot_confusion_matrix(cm, num_classes, labels, title='Confusion matrix', cmap=plt.cm.Blues):
	'''
	this function is needed to plot the confusion matrix with matplotlib
	'''
	plt.figure()
	plt.imshow(cm, interpolation='nearest', cmap=cmap)
	plt.title(title)
	plt.colorbar()
	tick_marks = np.arange(num_classes)
	plt.xticks(tick_marks, labels, rotation=45)
	plt.yticks(tick_marks, labels)
	plt.tight_layout()
	plt.ylabel('True label')
	plt.xlabel('Predicted label')
	plt.show()

