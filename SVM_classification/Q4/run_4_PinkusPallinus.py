import sys
sys.path.append('../')
import numpy as np 
from sklearn.model_selection import train_test_split
from sklearn.metrics import confusion_matrix, accuracy_score, precision_score, recall_score
import matplotlib.pyplot as plt
import argparse


#import the function to load the dataset
from common.Project_2_dataExtraction import *
#import the class which implements the model of SVM
from SVM_Multiclass import *
from Q1.hyper_parametres_trainer import *


arg_parse = argparse.ArgumentParser(description='')
arg_parse.add_argument('--C', dest='C', type=float, default=4, help='value of C')
arg_parse.add_argument('--gamma', dest='gamma', type=float, default=0.008, help='value of gamma')
arg_parse.add_argument('--treshold', dest='treshold', type=float, default=1e-7, help='precision to consider zero')
arg_parse.add_argument('--kernel', dest='kernel', type=str, default='RBF', help='kernel type: RBF/Poly')
args = arg_parse.parse_args()



#load all the entire mnist zalando dataset
X_all_labels, y_all_labels = load_mnist('../dataset/Data', kind='train')
#select from the entire dataset only the elemets which are pullovers (label == 2)
indexLabel2 = np.where((y_all_labels == 2))
xLabel2 =  X_all_labels[indexLabel2][:1000, :].astype('float64') / 255 #scale the images by 255 to have values between 0 and 1
yLabel2 = y_all_labels[indexLabel2][:1000].astype('float64') 
#select from the entire dataset only the elemets which are coat (label == 4)
indexLabel4 = np.where((y_all_labels == 4))
xLabel4 =  X_all_labels[indexLabel4][:1000, :].astype('float64') / 255
yLabel4 = y_all_labels[indexLabel4][:1000].astype('float64')
#select from the entire dataset only the elemets which are shirts (label == 6)
indexLabel6 = np.where((y_all_labels == 6))
xLabel6 =  X_all_labels[indexLabel6][:1000, :].astype('float64') / 255
yLabel6 = y_all_labels[indexLabel6][:1000].astype('float64')


X = np.append(np.append(xLabel2, xLabel4, axis=0), xLabel6, axis=0)
Y = np.append(np.append(yLabel2, yLabel4, axis=0), yLabel6, axis=0)
#prepare two portion of the dataset, one for training the model, the other to test it
X_train, X_test, y_train, y_test = train_test_split(X, Y, test_size=0.15, stratify=Y, shuffle=True, random_state=1703210)


#initialize the model with the hyperparametres
svm = SVM_Multiclass(args.C, args.gamma, kernel=args.kernel, treshold=args.treshold)


#train the model with the training set
classes, summaries, num_svms = svm.train(X_train, y_train)
opt_time = 0
opt_it = 0
kkt = 0
for s in summaries:
	opt_time += s["opt_time"]
	opt_it += s["opt_it"]
	kkt += s["kkt"]
kkt /= num_svms

#compute the output of the model for each elemets of the training set, the same for the test set
y_train_pred = svm.predict(X_train)
y_test_pred = svm.predict(X_test)

#compute machine learning performances
#recall = recall_score(y_test, y_test_pred, average=None)
#precision = precision_score(y_test, y_test_pred, average=None)
accu_test = accuracy_score(y_test, y_test_pred)
accu_train = accuracy_score(y_train, y_train_pred)
# Compute confusion matrix
cm = confusion_matrix(y_test, y_test_pred)
print("########################################")
print("hyper parameters values:")
print("C: ", args.C)
print("gamma: ", args.gamma)
print("kernel: ", args.kernel)
print("Accuracy on train set: ", accu_train*100)
print("Accuracy on test set: ", accu_test*100)
#print("precison: ", precision)
#print("recall: ", recall)
print("Optimization time: ", opt_time)
print("Optimization iterations: ", opt_it)
print("Violation of kkt condition: ", kkt)
print('Confusion matrix: ')
print(cm)
print("########################################")
# print("Initial value of the dual objective fun: ", summary["init_dual"])
# print("Dual objective fun at the optimum: ", summary["opt_dual"])
plot_confusion_matrix(cm, 3, classes)
