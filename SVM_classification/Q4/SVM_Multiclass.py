from SVM import *
import numpy as np

class SVM_Multiclass(object):
	def __init__(self, C, gamma, kernel='RBF', treshold=1e-7, verbose=False):
		'''
		constructor of the model
		C: penalty of misclassification
		gamma: parametre of the kernel function
		kernel: kind of kernel
		threshold: number to consider equal to zero
		verbose: boolean variable to have prints during computation 
		'''
		self.C = C
		self.gamma = gamma
		self.kernel = kernel
		self.verbose = verbose
		self.treshold = treshold

	def train(self, X, Y):
		'''
		one vs one version
		'''
		self.classes = np.unique(Y)
		Xs = []
		Ys = []
		#divide the element for each class
		for i in self.classes:
			Xs.append(X[Y == i])
			Ys.append(Y[Y == i])
		self.SVMs = []
		summaries = []
		for i in range(self.classes.shape[0]):
			for j in range(i+1, self.classes.shape[0]):
				print("train svm for classes", i+1, " vs ", j+1)
				svm = SVM(self.C, self.gamma, kernel=self.kernel, treshold=self.treshold, verbose=self.verbose)
				summary = svm.train(np.append(Xs[i], Xs[j], axis=0), np.append(Ys[i], Ys[j], axis=0))
				self.SVMs.append(svm)
				summaries.append(summary)
		return self.classes, summaries, len(self.SVMs)

	def predict(self, X):
		Y_pred = np.array([])
		conversion = []
		Y_preds = []
		for i in range(len(self.SVMs)):
			Y_pred_i, conv = self.SVMs[i].predict_all(X)
			conversion.append(conv)
			Y_preds.append(Y_pred_i)
		#each svm return -1 or 1 so we have to put the label associated to -1 or 1
		#we take the original label from the conversion array returned from the svm[i]
		for i in range(len(Y_preds)):
			Y_preds[i][Y_preds[i] == -1] = conversion[i][0]
			Y_preds[i][Y_preds[i] == 1] = conversion[i][1]
		Y_preds = np.array(Y_preds).T
		for i in range(Y_preds.shape[0]):
			y = np.array(Y_preds[i,:], dtype=np.int)
			counts = np.bincount(y)
			Y_pred = np.append(Y_pred, np.argmax(counts))
		return Y_pred
