import json
import numpy as np
import argparse


from Q1.hyper_parametres_trainer import *


arg_parse = argparse.ArgumentParser(description='')
arg_parse.add_argument('--file', dest='file', type=str, default='grid_search_result.json', help='output file of the grid search')
args = arg_parse.parse_args()

with open(args.file, 'r') as f:
	results = json.load(f)

gammas = np.array([])
Cs = np.array([])
training_accuracy = np.array([])
validation_accuracy = np.array([])

best_gamma = 0
best_C = 0
best_accuracy = 0

for key in results.keys():
	values = (key.replace("(","")).replace(")","").strip().split(",")
	gamma = values[0]
	C = values[1]
	
	gammas = np.append(gammas, gamma)
	Cs = np.append(Cs, C)

	train = results[key][0]
	validation = results[key][1]

	training_accuracy = np.append(training_accuracy, train)
	validation_accuracy = np.append(validation_accuracy, validation)

	if(validation > best_accuracy):
		best_accuracy = validation
		best_gamma = gamma
		best_C = C
		accuracys = (train, validation)

gammas = np.array(gammas, dtype=np.float32)
Cs = np.array(Cs, dtype=np.float32)
training_accuracy = np.array(training_accuracy, dtype=np.float32)
validation_accuracy = np.array(validation_accuracy, dtype=np.float32)

plot_heat_map(gammas, Cs, validation_accuracy)
print("the best model")
print("number of C: ", best_C)
print("gamma value: ", best_gamma)
print("error on train and validation set: ", accuracys)