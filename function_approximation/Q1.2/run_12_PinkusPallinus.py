import sys
sys.path.append('../')
import numpy as np 
import pandas as pd 
from sklearn.model_selection import train_test_split
import json
import argparse


#a library who implements some function over the models
from common.hyper_parametres_trainer import *
#definition of the model and all the function of the RBF neural network 
#are contained in file: ../common/RBF.py
from common.RBF import *


arg_parse = argparse.ArgumentParser(description='')
arg_parse.add_argument('--neurons', dest='neurons', type=int, default=47, help='# of neurons')
arg_parse.add_argument('--sigma', dest='sigma', type=float, default=0.9, help='value of sigma')
arg_parse.add_argument('--rho', dest='rho', type=float, default=1e-05, help='value of rho')
args = arg_parse.parse_args()


np.random.seed(1716359)

dataset_file = "../dataset/dataPoints.xlsx"

print("Loading the dataset...")
dataset = pd.read_excel(dataset_file, header=None).as_matrix()
#delete header of the excel file
dataset = np.delete(dataset,0,0)

print("dataset dimensions: "+str(dataset.shape[0]))
#split randomly the dataset in two set: one for train and one for test
np.random.shuffle(dataset)
print("splitting the dataset in training e test set...")
train_set, test_set = train_test_split(dataset, test_size=0.15)

print("test set dimensions: " +str(test_set.shape))
print("train set dimension: " +str(train_set.shape))
#create the model for a RBF
model = RBF(number_of_centres=args.neurons, input_dim=2, output_dim=1, sigma=args.sigma, rho=args.rho, method='BFGS', tollerance=1e-3, init="supervised", centres=None)
model.set_verbose(True)
#train the model on the full training set
model.train(train_set)

model.plot_approximation()
print("*****************************************")
print("error value on train set: ",model.mse(train_set))
print("error value on test set: ",model.mse(test_set))
print("*****************************************")

'''
uncomment for grid search
print("#######################################")
res = grid_search(model, train_set)
with open('../results/grid_search_result_RBF.json', 'w') as fp:
    json.dump(res, fp)
'''