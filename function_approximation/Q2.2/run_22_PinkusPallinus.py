import sys
sys.path.append('../')
import numpy as np 
import pandas as pd 
from sklearn.model_selection import train_test_split
import json
import argparse


#definition of the model and all the function of the RBF 
#are contained in file: ../common/RBF.py
from common.RBF import *


arg_parse = argparse.ArgumentParser(description='')
arg_parse.add_argument('--neurons', dest='neurons', type=int, default=47, help='# of neurons')
arg_parse.add_argument('--sigma', dest='sigma', type=float, default=0.9, help='value of sigma')
arg_parse.add_argument('--rho', dest='rho', type=float, default=1e-05, help='value of rho')
arg_parse.add_argument('--iteration', dest='iteration', type=int, default=30, help='# of changing ramdomly the parameters')
arg_parse.add_argument('--retrain', help='retrain the model', action='store_true')
args = arg_parse.parse_args()


np.random.seed(1716359)

dataset_file = "../dataset/dataPoints.xlsx"

print("Loading the dataset...")
dataset = pd.read_excel(dataset_file, header=None).as_matrix()
#delete header of the excel file
dataset = np.delete(dataset,0,0)

print("dataset dimensions: "+str(dataset.shape[0]))
#split randomly the dataset in two set: one for train and one for test
np.random.shuffle(dataset)
print("splitting the dataset in training e test set...")
train_set, test_set = train_test_split(dataset, test_size=0.15)

print("test set dimensions: " +str(test_set.shape))
print("train set dimension: " +str(train_set.shape))
#create the model for a RBF
model = RBF(number_of_centres=args.neurons, input_dim=2, output_dim=1, sigma=args.sigma, rho=args.rho, method='CG', tollerance=1e-4)
model.set_verbose(False)
train_set = np.array(train_set, dtype=np.float64)
best = 10
if(args.retrain):
	for i in range(args.iteration):
		print("iteration: ",i)
		#at each iteration the function extreme_learning select ramdomly the centres from the input data
		res = model.train_unsupervised(train_set)
		current = model.mse(test_set)
		print("avaluation on test set: ",current)
		if(current < best):
			best = current
			#save the best model
			res_best = res
			model.save_model()

	print("###########################################")
	print("number of neurons used: ",res_best["neurons"])
	print("value of sigma chosen: ",res_best["sigma"])
	print("value of rho chosen: ",res_best["rho"])
	print("norm of the gradient at the starting point: ",res_best["grad_start"])
	print("value function at the starting point: ",res_best["fun_start"])
	print("time spent to optimize the function: ",res_best["time"])
	print("optimization routine used: ",res_best["method"])
	print("number of function evaluation: ",res_best["fun_ev"])
	print("number of gradient evaluation: ",res_best["grad_ev"])
	print("number of iterations: ",res_best["num_it"])
	print("final message of the algorithm: ",res_best["mess"])
	print("norm of the grad at optimum: ",res_best["grad_opt"])
	print("value function at optimum: ",model.mse(train_set))
	print("###########################################")

model.load_model()

#plot the approximation of the function represented by the model
model.plot_approximation()
print("*****************************************")
print("error value on train set: ",model.mse(train_set))
print("error value on test set: ",model.mse(test_set))
print("*****************************************")
if(not args.retrain):
	print("if you want more information, rerun with \"--retrain\" option")
