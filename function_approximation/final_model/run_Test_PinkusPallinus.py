import sys
sys.path.append('../')
import numpy as np 
import pandas as pd 
import argparse


from common.ShallowNeuralNetwork import *

def get_Y_pred(X):
	model = ShallowNeuralNetwork(neurons=30, input_dim=2, output_dim=1, sigma=2.0, rho=1e-6)
	model.load_model()
	Y_pred = np.array([])
	for x in X:
		Y_pred = np.append(Y_pred, model.feed_forward(x))
	return Y_pred

arg_parse = argparse.ArgumentParser(description='best model done')
arg_parse.add_argument('--file', dest='file', type=str, default="dataPointsTest.xlsx", help='file of new samples')
args = arg_parse.parse_args()

print("PUT THE FILE IN THE SAME DIRECTORY OR PASS IT AS INPUT WITH OPTION \"--file\"")

print("Loading the dataset...")
dataset = pd.read_excel(args.file, header=None).as_matrix()
dataset = np.delete(dataset,0,0)
dataset = np.array(dataset, dtype=np.float64)

model = ShallowNeuralNetwork(neurons=30, input_dim=2, output_dim=1, sigma=2.0, rho=1e-6)
model.load_model()
print("mean squared error is: ", model.mse(dataset))
#print(get_Y_pred(dataset[:,0:2]))