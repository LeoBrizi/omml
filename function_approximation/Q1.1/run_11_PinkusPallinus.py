import sys
sys.path.append('../')
import numpy as np 
import pandas as pd 
from sklearn.model_selection import train_test_split
import json
import argparse


#a library who implements some function over the models
from common.hyper_parametres_trainer import *
#definition of the model and all the function of the shallow mlp 
#are contained in file: ../common/ShallowNeuralNetwork.py
from common.ShallowNeuralNetwork import *


arg_parse = argparse.ArgumentParser(description='')
arg_parse.add_argument('--neurons', dest='neurons', type=int, default=30, help='# of neurons')
arg_parse.add_argument('--sigma', dest='sigma', type=float, default=2.0, help='value of sigma')
arg_parse.add_argument('--rho', dest='rho', type=float, default=1e-6, help='value of rho')
args = arg_parse.parse_args()


np.random.seed(1703210)

dataset_file = "../dataset/dataPoints.xlsx"

print("Loading the dataset...")
dataset = pd.read_excel(dataset_file, header=None).as_matrix()
#delete header of the excel file
dataset = np.delete(dataset,0,0)

print("dataset dimensions: "+str(dataset.shape[0]))
#split randomly the dataset in two set: one for train and one for test
np.random.shuffle(dataset)
print("splitting the dataset in training e test set...")
train_set, test_set = train_test_split(dataset, test_size=0.15)

print("test set dimensions: " +str(test_set.shape))
print("train set dimension: " +str(train_set.shape))
#the split for the validation set is done in the k-cross fold validation
#create the model for a shallow MLP
model = ShallowNeuralNetwork(neurons=args.neurons, input_dim=2, output_dim=1, sigma=args.sigma, rho=args.rho, method='BFGS', tollerance=1e-3)
model.set_verbose(True)
#train the model on the full training set
train_set = np.array(train_set, dtype=np.float64)
model.train(train_set)
#plot the approximation of the function represented by the model
model.plot_approximation(definition=50)
print("*****************************************")
print("error value on train set: ",model.mse(train_set))
print("error value on test set: ",model.mse(test_set))
print("*****************************************")
'''
uncomment for grid search
print("#######################################")
res = grid_search(model, train_set)
with open('../results/grid_search_result.json', 'w') as fp:
    json.dump(res, fp)
'''