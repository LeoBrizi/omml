import sys
sys.path.append('../')
import json
import numpy as np
import argparse


from common.hyper_parametres_trainer import *


arg_parse = argparse.ArgumentParser(description='')
arg_parse.add_argument('--file', dest='file', type=str, default='grid_search_result.json', help='output file of the grid search')
args = arg_parse.parse_args()

with open(args.file, 'r') as f:
    results = json.load(f)

rhos = np.array([])
sigmas = np.array([])
Ns = np.array([])
training_errors = np.array([])
validation_errors = np.array([])

best_sigma = 0
best_rho = 0
best_N = 0
best_error = 10

for key in results.keys():
	values = (key.replace("(","")).replace(")","").strip().split(",")
	sigma = values[0]
	N = values[1]
	rho = values[2]
	
	sigmas = np.append(sigmas, sigma)
	Ns = np.append(Ns, N)
	rhos = np.append(rhos, rho)

	train = results[key][0]
	validation = results[key][1]

	training_errors = np.append(training_errors, train)
	validation_errors = np.append(validation_errors, validation)

	if(validation < best_error):
		best_error = validation
		best_sigma = sigma
		best_rho = rho
		best_N = N
		errors_b = (train, validation)

rhos = np.array(rhos, dtype=np.float32)
sigmas = np.array(sigmas, dtype=np.float32)
Ns = np.array(Ns, dtype=np.float32)
training_errors = np.array(training_errors, dtype=np.float32)
validation_errors = np.array(validation_errors, dtype=np.float32)

plot_heat_map(sigmas, Ns, rhos, training_errors)
print("the best model")
print("number of neurons: ",best_N)
print("sigma value: ",best_sigma)
print("rho value: ",best_rho)
print("error on train and validation set: ", errors_b)