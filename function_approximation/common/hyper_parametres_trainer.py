from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt
import numpy as np 

'''
this library is designed to work with model which implemets the following functions:
-mse: mean square error over a dataset
-train: train the model with a dataset
-set_Hyper_parmas: change the hyper parameters of the model
'''

def divide_data(data, times):
	#function to divide the dataset in small set of (data.shape[0]/times) elements 
	step = int(data.shape[0]/times)
	pools = np.zeros((times,step,data.shape[1]))
	for i in range(times):
		pools[i] = data[(i*step):(i*step)+step , :]
	return pools

def K_cross_val_score(model, data, cv):
	#k-cross validation for evaluation of hyperparameters
	#it returns the average of the training error and the average of the validation erros
	validation_error = np.zeros(cv)
	training_error = np.zeros(cv)
	#get the sets from the original dataset 
	data_pools = divide_data(data, cv)
	for i in range(cv):
		#use this set for evaluate the hyperparameters
		validation_set = data_pools[i]
		#use this set for training the parameters
		training_set = np.delete(data_pools, i, axis=0)
		training_set = training_set.reshape(training_set.shape[0]*training_set.shape[1], data.shape[1])
		#train the model
		model.train(training_set)
		#evaluate the model on train and validation
		validation_error[i] = model.mse(validation_set)
		training_error[i] = model.mse(training_set)
	avg_validation_error = np.average(validation_error)
	avg_training_error = np.average(training_error)
	return avg_training_error, avg_validation_error


def grid_search(model, training_set):
	#it make a grid_search on the possibile values for the hyperparameters
	#the function returns a dictionary where the key is the hyperparametes
	#and the value is the values of the errors (average_train, average_validation, test)
	result = {}
	#range for sigma 0.5 to 1.5 with step 0.02
	for sigma in range(50, 150, 20):
		sigma = sigma/100
		for n in range(2, 80, 7):
			#range for rho 10^-5 to 10^-3 with step 10^-5)
			for rho in range(1, 100, 1):
				rho = rho/(10**5)
				print("HYPER PARAMETERS: sigma: ",sigma," neurons: ",n," rho: ",rho)
				#set the new hyper parameters of the model
				model.set_Hyper_parmas(N=n, sigma=sigma, rho=rho)
				#run k-cross validation for these hyper param
				avg_train_error, avg_val_error = K_cross_val_score(model, training_set, cv=3)
				result[str((sigma, n, rho))] = (float(avg_train_error), float(avg_val_error))
				print("ERRORS VALUES: avg_train_error: ",avg_train_error," avg_val_error: ",avg_val_error)
	return result
	
def plot_heat_map(X,Y,Z,C):
	#plot a 4 dimensional function with a heat map
	#this needed for better understand how choose better hyper parameters
	fig = plt.figure()
	ax = fig.add_subplot(111, projection='3d')
	img = ax.scatter(X, Y, Z, c=C, cmap=plt.hot())
	fig.colorbar(img)
	plt.show()

#future work: use a better algorithm for the hyper parameters searching and use also the time of optimization
def genetic_algorith(model, training_set):
	raise Exception("method not yet implemented")

def simulated_annealing(model, training_set):
	raise Exception("method not yet implemented")
