import numpy as np
from scipy.optimize import minimize
import time
from sklearn.model_selection import cross_val_score

np.random.seed(1703210)

class ShallowNeuralNetwork(object):

	def __init__(self, neurons, input_dim, output_dim, sigma, rho, method=None, tollerance=None, mean=0, var=0.5):
		#some attribute of the shallow MLP
		'''
		neurons: number of neurons contained in the layer
		input_dim: dimension of the input
		output_dim: dimension of the output
		sigma: slope of the activaction function
		rho: regularize factor
		method: method used for minimize the error function
		mean: mean of the gaussian used to initialize the weighs of the network
		var: the variace of the gaussian used to initialize the weighs of the network
		verbose: boolean variable to have prints during computation
		tollerance: tollerance for the minimizing algorith
		W, bias, v: parameters of the model
		'''
		self.neurons = neurons
		self.input_dim = input_dim
		self.output_dim = output_dim
		self.sigma = sigma
		self.rho = rho
		self.method = method
		self.mean = mean
		self.var = var
		self.verbose = False
		self.tollerance = tollerance
		#construct the model 
		self.W = np.random.normal(mean, var, (self.neurons, self.input_dim))			
		self.bias = np.random.normal(mean, var, (self.neurons, 1))
		self.v = np.random.normal(mean, var, (self.neurons, self.output_dim))

	def save_model(self):
		print("saving the model...")
		np.save('W.npy', self.W)
		np.save('bias.npy', self.bias)
		np.save('v.npy', self.v)

	def load_model(self):
		import os
		print("loading the model...")
		if os.path.exists('W.npy') and os.path.exists('bias.npy') and os.path.exists('v.npy'):
			self.W = np.load('W.npy')
			self.bias = np.load('bias.npy')
			self.v = np.load('v.npy')
		else:
			print("trained model not found, you have to put the files in the same directory")

	def __activation_fun(self, t):
		# activation function of the neurons of the hidden layer
		return (np.exp(2*self.sigma * t) - 1) / (np.exp(2*self.sigma * t) + 1)
	def __D_activation_fun(self,t):
		#derivative of the activation function
		return (4*self.sigma*np.exp(2*self.sigma*t)) / ( (np.exp(2*self.sigma*t) + 1)**2 )
	
	def feed_forward(self, x):
		#it returns output of the model for a single input
		o1 = np.dot(self.W, x)
		o1 = o1 + np.transpose(self.bias)
		#output first layer
		o1 = self.__activation_fun(o1)
		#output second layer
		o2 = np.dot(o1, self.v)
		return o2

	def mse(self, data):
		#it returns the mean square error function of the model for some data taken in input
		mse = 0
		P = data.shape[0]
		for dato in data:
			x = np.array(dato[0:2], dtype=np.float64)
			#x = x.reshape(self.input_dim, 1)
			y = dato[2]
			err = (self.feed_forward(x) - y)
			mse += err * err
		mse = (mse / (2 * P)) 
		return float(mse)

	def reg_mse(self, data):
		mse = self.mse(data)
		params = np.append(np.append(self.v.flatten(), self.bias.flatten()), self.W.flatten())
		return mse + self.rho*np.dot(np.transpose(params), params)

	def fun_grad(self, data):
		#it returns the gradient of the mean square error function respect to each parameters of the model
		P = data.shape[0]

		d_mse_d_v = np.zeros(self.v.shape)
		d_mse_d_b = np.zeros(self.bias.shape)
		d_mse_d_W = np.zeros(self.W.shape)

		for dato in data:
			x = np.array(dato[0:self.input_dim], dtype=np.float64)
			x = x.reshape(self.input_dim, 1)
			y = dato[self.input_dim]

			o = np.dot(np.transpose(self.v), self.__activation_fun(np.dot(self.W, x) + self.bias)) - y
			#derivative of the activaction function computed in Wx+b
			d = self.__D_activation_fun(np.dot(self.W, x) + self.bias) 

			d_mse_d_v = d_mse_d_v + (o * self.__activation_fun(np.dot(self.W, x) + self.bias))
			d_mse_d_b = d_mse_d_b + (o * d * self.v)	#d*v is pointwise
			
			d_W_temp = np.zeros(self.W.shape)
			for i in range(d_W_temp.shape[1]):
				#v is a coloum, d is a coloum, v*d is pointwise, x[i] is a scalar
				d_W_temp[:, i] = np.transpose(self.v * d * x[i])	
			d_mse_d_W = d_mse_d_W + (o * d_W_temp)

		#sum the derivative of the regulation factor
		d_reg_err_d_v = d_mse_d_v/P + 2 * self.rho * self.v
		d_reg_err_d_b = d_mse_d_b/P + 2 * self.rho * self.bias
		d_reg_err_d_W = d_mse_d_W/P + 2 * self.rho * self.W

		#so far the partial gradients has the same dimension of the parameters to have a simpler computation
		#trasform the gradient in a vector of dimension equal to the summ of the all parameters  
		grad = np.append(np.append(d_reg_err_d_v.flatten(), d_reg_err_d_b.flatten()), d_reg_err_d_W.flatten())
		return grad


	def train(self, data):
		#function to find the optimal parameters 
		#that minimize the regularized mean square error over a dataset taken in input

		def unwrap_params(params):
			#the function minimize of the scipy.optimize packet wants a function with a single input
			#this function needs to unwrap all the parameter of the model from the single input
			v = params[0 : self.neurons * self.output_dim]
			v = v.reshape((self.neurons, self.output_dim))
			b = params[self.neurons * self.output_dim : self.neurons * self.output_dim + self.neurons]
			b = b.reshape((self.neurons, 1))
			W = params[self.neurons * self.output_dim + self.neurons : ]
			W = W.reshape((self.neurons, self.input_dim))
			return v, b, W

		def fun_optimize(params):
			# function that must be optimized
			#it return the value of the regularized mse over the data taken in input
			params = np.array(params)
			v, b, W = unwrap_params(params)
			regularized_err = 0
			P = data.shape[0]
			for dato in data:
				x = np.array(dato[0:2], dtype=np.float64)
				x = x.reshape(self.input_dim, 1)
				y = dato[2]
				f_d = np.dot(np.transpose(v), self.__activation_fun(np.dot(W, x) + b))
				err = (f_d - y)
				regularized_err += err * err
			norm = np.transpose(params).dot(params)
			regularized_err = (regularized_err / (2 * P)) + self.rho * norm
			return regularized_err

		def fun_grad(params):
			#it is equal of the previous fun_grad 
			#but it computes the gradient for the parameters taken in input not the one of the model
			params = np.array(params)
			v, b, W = unwrap_params(params)
			P = data.shape[0]

			d_mse_d_v = np.zeros(v.shape)
			d_mse_d_b = np.zeros(b.shape)
			d_mse_d_W = np.zeros(W.shape)

			for dato in data:
				x = np.array(dato[0:self.input_dim], dtype=np.float64)
				x = x.reshape(self.input_dim, 1)
				y = dato[self.input_dim]
				o = np.dot(np.transpose(v), self.__activation_fun(np.dot(W, x) + b)) - y
				d = self.__D_activation_fun(np.dot(W, x) + b)
				d_mse_d_v = d_mse_d_v + (o * self.__activation_fun(np.dot(W, x) + b))
				d_mse_d_b = d_mse_d_b + (o * d * v)

				d_W_temp = np.zeros(W.shape)
				for i in range(d_W_temp.shape[1]):
					d_W_temp[:, i] = np.transpose(v * d * x[i])	
				d_mse_d_W = d_mse_d_W + (o * d_W_temp)

			d_reg_err_d_v = d_mse_d_v/P + 2 * self.rho * v
			d_reg_err_d_b = d_mse_d_b/P + 2 * self.rho * b
			d_reg_err_d_W = d_mse_d_W/P + 2 * self.rho * W

			grad = np.append(np.append(d_reg_err_d_v.flatten(), d_reg_err_d_b.flatten()), d_reg_err_d_W.flatten())
			return grad

		#where the algorithm starts, it starts from the actual parameter of the model
		starting_point = np.append(np.append(self.v.flatten(), self.bias.flatten()), self.W.flatten())
		start_grad = str(np.transpose(fun_grad(starting_point)).dot(fun_grad(starting_point)))
		start_fun_value = fun_optimize(starting_point)

		start = time.time()

		res = minimize(fun_optimize, starting_point, method=self.method, jac=fun_grad, tol=self.tollerance)

		running_time=time.time()-start
		#save the optimal parameters founded as the parameters of the model
		self.v, self.bias, self.W = unwrap_params(res.x)

		if self.verbose:
			print("###########################################")
			print("number of neurons used: ",self.neurons)
			print("value of sigma chosen: ",self.sigma)
			print("value of rho chosen: ",self.rho)
			print("norm of the gradient at the starting point: ",start_grad)
			print("value function at the starting point: ",start_fun_value)
			print("time spent to optimize the function: ",str(running_time))
			print("optimization routine used: ",self.method)
			print("number of function evaluation: ",res.nfev)
			print("number of gradient evaluation: ",res.njev)
			print("number of iterations: ",res.nit)
			print("final message of the algorithm: ",res.message)
			print("norm of the grad at optimum: ",np.transpose(fun_grad(res.x)).dot(fun_grad(res.x)))
			print("value function at optimum: ",fun_optimize(res.x))
			print("###########################################")

	def extreme_learning(self, data):
		#fix all the parameters of the first layer and optimize the function only respect to v
		#use a different initialization for the parameters
		self.W =  np.random.rand(self.neurons, self.input_dim)	
		self.bias = np.random.rand(self.neurons, 1)				
		self.v = np.random.rand(self.neurons, self.output_dim)	

		def unwrap_params(params):
			v = params[0 : self.neurons * self.output_dim]
			v = v.reshape((self.neurons, self.output_dim))
			return v

		def fun_optimize(params):
			# function that must be optimized
			#it is function only of v, W and b are not taken in input
			params = np.array(params)
			v = unwrap_params(params)
			regularized_err = 0
			P = data.shape[0]
			for dato in data:
				x = np.array(dato[0:2], dtype=np.float64)
				x = x.reshape(self.input_dim, 1)
				y = dato[2]
				f_d = np.dot(np.transpose(v), self.__activation_fun(np.dot(self.W, x) + self.bias))
				err = (f_d - y)
				regularized_err += err * err
			norm = np.transpose(params).dot(params)
			regularized_err = (regularized_err / (2 * P)) + self.rho * norm
			return regularized_err

		def fun_grad(params):
			#it returns the gradient of regularized mse only respect to v
			params = np.array(params)
			v = unwrap_params(params)
			P = data.shape[0]
			d_mse_d_v = np.zeros(v.shape)

			for dato in data:
				x = np.array(dato[0:self.input_dim], dtype=np.float64)
				x = x.reshape(self.input_dim, 1)
				y = dato[self.input_dim]
				o = np.dot(np.transpose(v), self.__activation_fun(np.dot(self.W, x) + self.bias)) - y
				d_mse_d_v = d_mse_d_v + (o * self.__activation_fun(np.dot(self.W, x) + self.bias))

			d_reg_err_d_v = d_mse_d_v/P + 2 * self.rho * v

			grad = d_reg_err_d_v.flatten()
			return grad


		starting_point = self.v.flatten()

		start_grad = str(np.transpose(fun_grad(starting_point)).dot(fun_grad(starting_point)))
		start_fun_value = fun_optimize(starting_point)

		start = time.time()

		res = minimize(fun_optimize, starting_point, method=self.method, jac=fun_grad, tol=1e-3)

		running_time = time.time() - start
		self.v = unwrap_params(res.x)

		if self.verbose:
			print("###########################################")
			print("number of neurons used: ",self.neurons)
			print("value of sigma chosen: ",self.sigma)
			print("value of rho chosen: ",self.rho)
			print("norm of the gradient at the starting point: ",start_grad)
			print("value function at the starting point: ",start_fun_value)
			print("time spent to optimize the function: ",str(running_time))
			print("optimization routine used: ",self.method)
			print("number of function evaluation: ",res.nfev)
			print("number of gradient evaluation: ",res.njev)
			print("number of iterations: ",res.nit)
			print("final message of the algorithm: ",res.message)
			print("norm of the grad at optimum: ",np.transpose(fun_grad(res.x)).dot(fun_grad(res.x)))
			print("value function at optimum: ",fun_optimize(res.x))
			print("###########################################")

		res = {"neurons": self.neurons, "sigma": self.sigma, "rho": self.rho, "grad_start": start_grad, "fun_start": start_fun_value,
			"time":str(running_time), "method": self.method, "fun_ev": res.nfev, "grad_ev": res.njev, "num_it": res.nit,
			"mess": res.message, "grad_opt": np.transpose(fun_grad(res.x)).dot(fun_grad(res.x))}
		return res

	def decomposition_method(self, data):
		#function to optimize the regularized mse with the two block decomposition method

		def unwrap_params_for_W_b(params):
			b = params[0 : self.neurons]
			b = b.reshape((self.neurons, 1))
			W = params[self.neurons : ]
			W = W.reshape((self.neurons, self.input_dim))
			return b, W

		def fun_optimize_respect_to_W_and_b(params):
			#it is function only of the first layer parameters
			#v is fixed
			params = np.array(params)
			b, W = unwrap_params_for_W_b(params)
			regularized_err = 0
			P = data.shape[0]
			for dato in data:
				x = np.array(dato[0:2], dtype=np.float64)
				x = x.reshape(self.input_dim, 1)
				y = dato[2]
				f_d = np.dot(np.transpose(self.v), self.__activation_fun(np.dot(W, x) + b))
				err = (f_d - y)
				regularized_err += err * err
			norm = np.transpose(params).dot(params)
			regularized_err = (regularized_err / (2 * P)) + self.rho * norm
			return regularized_err

		def fun_grad_respect_to_W_and_b(params):
			#gradient of the function respect to the first layer parameters
			#v is considered fixed
			params = np.array(params)
			b, W = unwrap_params_for_W_b(params)
			P = data.shape[0]

			d_mse_d_b = np.zeros(b.shape)
			d_mse_d_W = np.zeros(W.shape)

			for dato in data:
				x = np.array(dato[0:self.input_dim], dtype=np.float64)
				x = x.reshape(self.input_dim, 1)
				y = dato[self.input_dim]
				o = np.dot(np.transpose(self.v), self.__activation_fun(np.dot(W, x) + b)) - y
				d = self.__D_activation_fun(np.dot(W, x) + b)
				d_mse_d_b = d_mse_d_b + (o * d * self.v)
				d_W_temp = np.zeros(W.shape)
				for i in range(d_W_temp.shape[1]):
					d_W_temp[:, i] = np.transpose(self.v * d * x[i])
				d_mse_d_W = d_mse_d_W + (o * d_W_temp)
			d_reg_err_d_b = d_mse_d_b/P + 2 * self.rho * b
			d_reg_err_d_W = d_mse_d_W/P + 2 * self.rho * W

			grad = np.append(d_reg_err_d_b.flatten(), d_reg_err_d_W.flatten())
			return grad

		def unwrap_params_for_v(params):
			v = params[0 : self.neurons * self.output_dim]
			v = v.reshape((self.neurons, self.output_dim))
			return v

		def fun_optimize_respect_to_v(params):
			#the same respect to v
			params = np.array(params)
			v = unwrap_params_for_v(params)
			regularized_err = 0
			P = data.shape[0]
			for dato in data:
				x = np.array(dato[0:2], dtype=np.float64)
				x = x.reshape(self.input_dim, 1)
				y = dato[2]
				f_d = np.dot(np.transpose(v), self.__activation_fun(np.dot(self.W, x) + self.bias))
				err = (f_d - y)
				regularized_err += err * err
			norm = np.transpose(params).dot(params)
			regularized_err = (regularized_err / (2 * P)) + self.rho * norm
			return regularized_err

		def fun_grad_respect_to_v(params):
			#the same respect to v
			params = np.array(params)
			v = unwrap_params_for_v(params)
			P = data.shape[0]
			d_mse_d_v = np.zeros(v.shape)

			for dato in data:
				x = np.array(dato[0:self.input_dim], dtype=np.float64)
				x = x.reshape(self.input_dim, 1)
				y = dato[self.input_dim]
				o = np.dot(np.transpose(v), self.__activation_fun(np.dot(self.W, x) + self.bias)) - y
				d_mse_d_v = d_mse_d_v + (o * self.__activation_fun(np.dot(self.W, x) + self.bias))

			d_reg_err_d_v = d_mse_d_v/P + 2 * self.rho * v

			grad = d_reg_err_d_v.flatten()
			return grad

		start_grad = str(np.transpose(self.fun_grad(data)).dot(self.fun_grad(data)))
		start_fun_value = self.reg_mse(data)

		stopping_criterion = False
		#dictionary to keep track of the results
		res = {'iterations':0, 'nfev':0, 'njev':0}
		#criteriors to stop the algorithm
		epsilon_W_b = 5e-6 #min value of the gradient respect to W and b
		epsilon_v = 1e-9  #min value of the gradient respect to v
		max_iteration = 30  #max iteration of the algorithm
		#tollerance for the optimization methods, it increases with the number of iterations
		tollerance = 1

		start = time.time()

		while( not stopping_criterion):
			print("number of iterations: ", res['iterations'])

			#minimizing respect to W,b
			starting_point = np.append(self.bias.flatten(), self.W.flatten())
			partial_res1 = minimize(fun_optimize_respect_to_W_and_b, starting_point, method='BFGS', jac=fun_grad_respect_to_W_and_b, tol=tollerance)

			self.bias, self.W = unwrap_params_for_W_b(partial_res1.x)
			res['nfev'] += partial_res1.nfev
			res['njev'] += partial_res1.njev

			#minimizing respect to v
			starting_point = self.v.flatten()
			partial_res2 = minimize(fun_optimize_respect_to_v, starting_point, method='CG', jac=fun_grad_respect_to_v, tol=tollerance)

			self.v = unwrap_params_for_v(partial_res2.x)
			res['nfev'] += partial_res2.nfev
			res['njev'] += partial_res2.njev

			norm_grad_W_b = np.transpose(fun_grad_respect_to_W_and_b(partial_res1.x)).dot(fun_grad_respect_to_W_and_b(partial_res1.x))
			norm_grad_v = np.transpose(fun_grad_respect_to_v(partial_res2.x)).dot(fun_grad_respect_to_v(partial_res2.x))

			res['iterations'] += 1
			if res['iterations']%2 == 0 and tollerance > 1e-5:
				tollerance = tollerance/10
			if self.verbose:
				print("norm of the grad for W and b: ", norm_grad_W_b)
				print("norm of the grad for v: ", norm_grad_v)

			stopping_criterion = ((norm_grad_W_b < epsilon_W_b) and (norm_grad_v < epsilon_v)) or (res['iterations'] > max_iteration)

		running_time = time.time() - start

		if self.verbose:
			print("###########################################")
			print("number of neurons used: ",self.neurons)
			print("value of sigma chosen: ",self.sigma)
			print("value of rho chosen: ",self.rho)
			print("norm of the gradient at the starting point: ",start_grad)
			print("value function at the starting point: ",start_fun_value)
			print("time spent to optimize the function: ",str(running_time))
			print("optimization routine used for W and b: BFGS")
			print("optimization routine used for v: CG")
			print("number of function evaluation: ",res["nfev"])
			print("number of gradient evaluation: ",res["njev"])
			print("number of iterations: ",res["iterations"])
			print("norm of the grad at optimum: ",np.transpose(self.fun_grad(data)).dot(self.fun_grad(data)))
			print("value function at optimum: ",self.reg_mse(data))
			print("###########################################")

	def set_Hyper_parmas(self, N, sigma, rho):
		#function to set the hyper parameters and reinitialize all the parameters
		#it is designed for the searching of the best hyperparameters
		self.neurons = N
		self.sigma = sigma
		self.rho = rho
		self.W = np.random.normal(self.mean, self.var, (self.neurons, self.input_dim))				
		self.bias = np.random.normal(self.mean, self.var, (self.neurons, 1))
		self.v = np.random.normal(self.mean, self.var, (self.neurons, self.output_dim))

	def plot_approximation(self, definition=100, title='Plotting of the function'):
		#plot the function approximated from the model
		#definition is the number of point used to plot the function
		import matplotlib.pyplot as plt
		from mpl_toolkits.mplot3d import Axes3D
		# create the object
		fig = plt.figure()
		ax = plt.axes(projection='3d')
		# create the grid
		x1 = np.linspace(-2, 2, definition)   
		x2 = np.linspace(-1, 1, definition)
		#create the grid for the plot
		X1, X2 = np.meshgrid(x1, x2) 
		Z = np.zeros((definition, definition))
		#evaluate the function on the grid
		for i in range(definition):
			for j in range(definition):
				Z[i,j] = self.feed_forward(np.array([X1[i,j],X2[i,j]]))
		
		ax.plot_surface(X1, X2, Z, rstride=1, cstride=1,cmap='viridis', edgecolor='none')
		ax.set_xlabel('x1')
		ax.set_ylabel('x2')
		ax.set_zlabel('z')
		ax.set_title(title)
		plt.show()

	def set_verbose(self, verbose):
		#function to have prints during the training, 
		#for searching of the hyperparameter sigma, rho and number of neuron 
		#is good to have this disabled
		self.verbose = verbose

	def set_method(self, method):
		#function to change the optimization method
		self.method = method