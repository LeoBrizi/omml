import numpy as np
from scipy.optimize import minimize
import time

np.random.seed(1716359)

class RBF(object):

	def __init__(self, number_of_centres, input_dim, output_dim, sigma, rho, method=None, tollerance=None, init="supervised", centres=None):
		#some attribute of the 
		'''
		number_of_centres: number of neurons contained in the layer
		input_dim: dimension of the input
		output_dim: dimension of the output
		sigma: spread of the gaussian
		rho: regularize factor
		method: method used for minimize the error function
		verbose: boolean variable to have prints during computation
		tollerance: tollerance for the minimizing algorith
		C, v: parameters of the model
		this class is very similar to the ShallowNeuralNetwork and it implements some common functions
		'''
		self.neurons = number_of_centres
		self.input_dim = input_dim
		self.output_dim = output_dim
		self.sigma = sigma
		self.rho = rho
		self.method = method
		self.init = init
		self.verbose = False
		self.tollerance = tollerance

		if centres == None:
			self.centres_not_initialized = True

		if(self.init == "supervised"):
			self.C = np.random.rand(self.neurons, self.input_dim)
			self.v = np.random.rand(self.neurons, self.output_dim)
		else:
			#there is the possiblility to create the model choosing the center and pass them as input
			self.C = centres
			self.v = np.random.rand(self.neurons, self.output_dim)

	def save_model(self):
		print("saving the model...")
		np.save('C.npy', self.C)
		np.save('v.npy', self.v)

	def load_model(self):
		import os
		print("loading the model...")
		if os.path.exists('C.npy') and os.path.exists('v.npy'):
			self.C = np.load('C.npy')
			self.v = np.load('v.npy')
		else:
			print("trained model not found, you have to put the files in the same directory")

	def set_Hyper_parmas(self, N, sigma, rho, centres=None):
		#function to set the hyper parameters and reinitialize all the parameters
		#it is designed for the searching of the best hyperparameters
		self.neurons = N
		self.sigma = sigma
		self.rho = rho	
		if(self.init == "supervised"):
			self.C = np.random.rand(self.neurons, self.input_dim)
			self.v = np.random.rand(self.neurons, self.output_dim)
		else:
			self.C = centres
			self.v = np.random.rand(self.neurons, self.output_dim)

	def __kernel_function(self, t):
		#function for each neuron
		return np.exp(-(t / self.sigma)**2)

	def __D_kernel_function(self, t):
		#derivative of the kernel function
		return -((2 * t/self.sigma**2) * np.exp(-(t/self.sigma)**2))

	def __D_norm(self, x, C):
		#derivative of the norm of an input x respect to all the centres C
		grad = np.zeros(C.shape)
		for i in range(grad.shape[0]):
			norm = np.dot( x - C[i,:] , np.transpose(x - C[i,:]) )
			norm = np.sqrt(norm)
			grad[i,:] = (1 / norm) * (C[i] - x)
		return grad

	def output_function(self, X):
		#it returns output of the model for a single input
		#pairwise_dists is a matrix where a row is the distance for a input respect to all centres 
		pairwise_dists = np.sum(np.square(X[:, np.newaxis] - self.C[np.newaxis, :]), axis=-1)
		pairwise_dists = np.sqrt(pairwise_dists)
		o1 = self.__kernel_function(pairwise_dists)
		o2 = np.dot(o1, self.v)
		return o2

	def mse(self, data):
		#it returns the mean square error function of the model for some data taken in input
		train_set = np.array(data, dtype=np.float64)
		X = train_set[:,0:2]
		P = X.shape[0]
		Y = train_set[:,2].reshape(P, self.output_dim)
		error = self.output_function(X) - Y
		error = np.dot(np.transpose(error), error) / (2*P) 
		return error

	def fun_grad(self, data):
		#it returns the gradient of the mean square error function respect to each parameters of the model
		train_set = np.array(data,dtype=np.float64)
		P = data.shape[0]
		d_mse_d_C = np.zeros(self.C.shape)
		d_mse_d_v = np.zeros(self.v.shape)
		for dato in train_set:
			x = dato[0:2].reshape(1,2)
			y = dato[2]
			pairwise_dists = np.sum(np.square(x[:, np.newaxis] - self.C[np.newaxis, :]), axis=-1)
			pairwise_dists = np.sqrt(pairwise_dists) #returns the vector of the norm of input x respect to the all centres
			o1 = self.__kernel_function(pairwise_dists)
			o2 = np.dot(o1, self.v)
			d_mse_d_C = d_mse_d_C + (o2-y) * self.v * np.transpose(self.__D_kernel_function(pairwise_dists)) * self.__D_norm(x, self.C)
			d_mse_d_v = d_mse_d_v + (o2-y) * np.transpose(o1) 
		
		d_reg_err_d_v = d_mse_d_v/P + 2 * self.rho * self.v
		d_reg_err_d_C = d_mse_d_C/P + 2 * self.rho * self.C
		grad = np.append(d_reg_err_d_v.flatten(), d_reg_err_d_C.flatten())
		return grad

	def reg_error_function(self, data):
		#it returns the regularized mean square error function of the model for some data taken in input
		params = np.append(self.C.flatten(), self.v.flatten())
		error = self.mse(data)
		return error + self.rho * np.dot(params, params)

	def plot_approximation(self, definition=100, title='Plotting of the function'):
		#plot the function approximated from the model
		#definition is the number of point used to plot the function
		import matplotlib.pyplot as plt
		from mpl_toolkits.mplot3d import Axes3D
		# create the object
		fig = plt.figure()
		ax = plt.axes(projection='3d')
		# create the grid
		x1 = np.linspace(-2, 2, definition)   
		x2 = np.linspace(-1, 1, definition)

		X1, X2 = np.meshgrid(x1, x2) #create the grid for the plot
		Z = np.zeros((definition, definition))

		for i in range(definition):
			for j in range(definition):
				t = np.array([X1[i,j], X2[i,j]])
				t = t.reshape(1,2)
				Z[i,j] = self.output_function(t)
		
		ax.plot_surface(X1, X2, Z, rstride=1, cstride=1,cmap='viridis', edgecolor='none')

		ax.set_xlabel('x1')
		ax.set_ylabel('x2')
		ax.set_zlabel('z')
		ax.set_title(title)
		plt.show()

	def set_verbose(self, verbose):
		self.verbose = verbose
	
	def train(self, data):
		#function to find the optimal parameters 
		#that minimize the regularized mean square error over a dataset taken in input
		#it is very similar to the train function of the Shallow MLP (see ShallowNeuralNetwork.py)
		train_set = np.array(data,dtype=np.float64)
		X = train_set[:,0:2]
		P = X.shape[0]
		Y = train_set[:,2].reshape(P, self.output_dim)

		def unwrap_params(params):
			v = params[0 : self.neurons * self.output_dim]
			v = v.reshape((self.neurons, self.output_dim))
			C = params[self.neurons * self.output_dim : ]
			C = C.reshape((self.neurons, self.input_dim))
			return v, C

		def fun_optimize(params):
			# parse argument
			params = np.array(params).flatten()
			v, C = unwrap_params(params)
			pairwise_dists = np.sum(np.square(X[:, np.newaxis] - C[np.newaxis, :]), axis=-1)
			pairwise_dists = np.sqrt(pairwise_dists)
			o1 = self.__kernel_function(pairwise_dists)
			o2 = np.dot(o1, v)
			error = o2 - Y
			error = np.dot(np.transpose(error), error) / (2*P)
			return error + self.rho * np.dot(params, params)

		def fun_grad(params):
			params = np.array(params).flatten()
			v, C = unwrap_params(params)
			P = data.shape[0]

			d_mse_d_C = np.zeros(C.shape)
			d_mse_d_v = np.zeros(v.shape)
			for dato in train_set:
				x = dato[0:2].reshape(1,2)
				y = dato[2]
				pairwise_dists = np.sum(np.square(x[:, np.newaxis] - C[np.newaxis, :]), axis=-1)
				pairwise_dists = np.sqrt(pairwise_dists) 
				o1 = self.__kernel_function(pairwise_dists)
				o2 = np.dot(o1, v)
				d_mse_d_C = d_mse_d_C + (o2-y) * v * np.transpose(self.__D_kernel_function(pairwise_dists)) * self.__D_norm(x,C)
				d_mse_d_v = d_mse_d_v + (o2-y) * np.transpose(o1) 
			
			d_reg_err_d_v = d_mse_d_v/P + 2 * self.rho * v
			d_reg_err_d_C = d_mse_d_C/P + 2 * self.rho * C
			grad = np.append(d_reg_err_d_v.flatten(), d_reg_err_d_C.flatten())
			return grad

		starting_point = np.append(self.v.flatten(), self.C.flatten())
		start_fun_value = self.reg_error_function(data)
		start_grad = np.transpose(fun_grad(starting_point)).dot(fun_grad(starting_point))

		start = time.time()

		res = minimize(fun_optimize, starting_point, method=self.method, jac=fun_grad, tol=self.tollerance)

		running_time = time.time() - start

		self.v, self.C = unwrap_params(res.x)
		
		if self.verbose:
			print("###########################################")
			print("number of neurons used: ",self.neurons)
			print("value of sigma chosen: ",self.sigma)
			print("value of rho chosen: ",self.rho)
			print("norm of the gradient at the starting point: ",start_grad)
			print("value function at the starting point: ",start_fun_value)
			print("time spent to optimize the function: ",str(running_time))
			print("optimization routine used: ",self.method)
			print("number of function evaluation: ",res.nfev)
			print("number of gradient evaluation: ",res.njev)
			print("number of iterations: ",res.nit)
			print("final message of the algorithm: ",res.message)
			print("norm of the grad at optimum: ",np.transpose(fun_grad(res.x)).dot(fun_grad(res.x)))
			print("value function at optimum: ",self.reg_error_function(data))
			print("###########################################")

	def train_unsupervised(self, data):
		#in this kind of learning for the RBF neural network we choose from the dataset some 
		#points and we set them as center of our RBF then we optimize the error function
		#only respect to the weight of the output layer
		train_set = np.array(data,dtype=np.float64)
		X = train_set[:,0:2]
		P = X.shape[0]
		Y = train_set[:,2].reshape(P, self.output_dim)
		if self.centres_not_initialized:
			#select random centres from data and fix them
			indeces_of_centres = np.random.choice(X.shape[0], self.neurons)
			self.C = X[indeces_of_centres]

		def unwrap_params(params):
			v = params[0 : self.neurons * self.output_dim]
			v = v.reshape((self.neurons, self.output_dim))
			return v

		def fun_optimize(params):
			params = np.array(params).flatten()
			v = unwrap_params(params)
			pairwise_dists = np.sum(np.square(X[:, np.newaxis] - self.C[np.newaxis, :]), axis=-1)
			pairwise_dists = np.sqrt(pairwise_dists)
			o1 = self.__kernel_function(pairwise_dists)
			o2 = np.dot(o1, v)
			error = o2 - Y
			error = np.dot(np.transpose(error), error) / (2*P)
			return error + self.rho * np.dot(params, params)

		def fun_grad(params):
			params = np.array(params).flatten()
			v = unwrap_params(params)
			P = data.shape[0]

			d_mse_d_v = np.zeros(v.shape)
			for dato in train_set:
				x = dato[0:2].reshape(1,2)
				y = dato[2]
				pairwise_dists = np.sum(np.square(x[:, np.newaxis] - self.C[np.newaxis, :]), axis=-1)
				pairwise_dists = np.sqrt(pairwise_dists)  
				o1 = self.__kernel_function(pairwise_dists)
				o2 = np.dot(o1, v)
				d_mse_d_v = d_mse_d_v + (o2-y) * np.transpose(o1) 
			
			d_reg_err_d_v = d_mse_d_v/P + 2 * self.rho * v
			grad = d_reg_err_d_v.flatten()
			return grad

		starting_point = self.v.flatten()
		start_fun_value = self.reg_error_function(data)
		start_grad = np.transpose(fun_grad(starting_point)).dot(fun_grad(starting_point))

		start = time.time()

		res = minimize(fun_optimize, starting_point, method=self.method, jac=fun_grad, tol=self.tollerance)

		running_time = time.time() - start

		self.v = unwrap_params(res.x)
		if self.verbose:

			print("###########################################")
			print("number of neurons used: ",self.neurons)
			print("value of sigma chosen: ",self.sigma)
			print("value of rho chosen: ",self.rho)
			print("norm of the gradient at the starting point: ",start_grad)
			print("value function at the starting point: ",start_fun_value)
			print("time spent to optimize the function: ",str(running_time))
			print("optimization routine used: ",self.method)
			print("number of function evaluation: ",res.nfev)
			print("number of gradient evaluation: ",res.njev)
			print("number of iterations: ",res.nit)
			print("final message of the algorithm: ",res.message)
			print("norm of the grad at optimum: ",np.transpose(fun_grad(res.x)).dot(fun_grad(res.x)))
			print("value function at optimum: ",self.reg_error_function(data))
			print("###########################################")
			
		res = {"neurons": self.neurons, "sigma": self.sigma, "rho": self.rho, "grad_start": start_grad, "fun_start": start_fun_value,
			"time":str(running_time), "method": self.method, "fun_ev": res.nfev, "grad_ev": res.njev, "num_it": res.nit,
			"mess": res.message, "grad_opt": np.transpose(fun_grad(res.x)).dot(fun_grad(res.x))}
		return res

	def decomposition_method(self, data):
		#this methos is very similar for the Shallow MLP
		#we optimize alternately respect to the center and respect to the v
		train_set = np.array(data,dtype=np.float64)
		X = train_set[:,0:2]
		P = X.shape[0]
		Y = train_set[:,2].reshape(P, self.output_dim)

		def unwrap_params_for_C(params):
			C = params.reshape((self.neurons, self.input_dim))
			return C
		
		def unwrap_params_for_v(params):
			v = params.reshape((self.neurons, self.output_dim))
			return v

		def fun_optimize_for_C(params):
			params = np.array(params).flatten()
			C = unwrap_params_for_C(params)
			pairwise_dists = np.sum(np.square(X[:, np.newaxis] - C[np.newaxis, :]), axis=-1)
			pairwise_dists = np.sqrt(pairwise_dists)
			o1 = self.__kernel_function(pairwise_dists)
			o2 = np.dot(o1, self.v)
			error = o2 - Y
			error = np.dot(np.transpose(error), error) / (2*P)
			return error + self.rho * np.dot(params, params)

		def fun_grad_for_C(params):
			params = np.array(params).flatten()
			C = unwrap_params_for_C(params)
			P = data.shape[0]

			d_mse_d_C = np.zeros(C.shape)
			for dato in train_set:
				x = dato[0:2].reshape(1,2)
				y = dato[2]
				pairwise_dists = np.sum(np.square(x[:, np.newaxis] - C[np.newaxis, :]), axis=-1)
				pairwise_dists = np.sqrt(pairwise_dists) 
				o1 = self.__kernel_function(pairwise_dists)
				o2 = np.dot(o1, self.v)
				d_mse_d_C = d_mse_d_C + (o2-y) * self.v * np.transpose(self.__D_kernel_function(pairwise_dists)) * self.__D_norm(x,C)

			d_reg_err_d_C = d_mse_d_C/P + 2 * self.rho * C
			grad = d_reg_err_d_C.flatten()
			return grad

		def fun_optimize_for_v(params):
			params = np.array(params).flatten()
			v = unwrap_params_for_v(params)
			pairwise_dists = np.sum(np.square(X[:, np.newaxis] - self.C[np.newaxis, :]), axis=-1)
			pairwise_dists = np.sqrt(pairwise_dists)
			o1 = self.__kernel_function(pairwise_dists)
			o2 = np.dot(o1, v)
			error = o2 - Y
			error = np.dot(np.transpose(error), error) / (2*P)
			return error + self.rho * np.dot(params, params)

		def fun_grad_for_v(params):
			params = np.array(params).flatten()
			v = unwrap_params_for_v(params)
			P = data.shape[0]

			d_mse_d_v = np.zeros(v.shape)
			for dato in train_set:
				x = dato[0:2].reshape(1,2)
				y = dato[2]
				pairwise_dists = np.sum(np.square(x[:, np.newaxis] - self.C[np.newaxis, :]), axis=-1)
				pairwise_dists = np.sqrt(pairwise_dists) 
				o1 = self.__kernel_function(pairwise_dists)
				o2 = np.dot(o1, v)
				d_mse_d_v = d_mse_d_v + (o2-y) * np.transpose(o1) 
			
			d_reg_err_d_v = d_mse_d_v/P + 2 * self.rho * v
			grad = d_reg_err_d_v.flatten()
			return grad

		start_fun_value = self.reg_error_function(data)
		start_grad = np.transpose(self.fun_grad(data)).dot(self.fun_grad(data))

		stopping_criterion = False
		#dictionary to keep track of the results
		res = {'iterations':0, 'nfev':0, 'njev':0}
		start = time.time()
		#criteriors to stop the algorithm
		epsilon_C = 1e-8
		epsilon_v = 1e-10
		max_iteration = 10
		tollerance = 1e-2

		while( not stopping_criterion):
			print("number of iterations: ", res['iterations'])
			#minimizing respect to C
			starting_point = self.C.flatten()
			partial_res1 = minimize(fun_optimize_for_C, starting_point, method='BFGS', jac=fun_grad_for_C, tol=tollerance)

			self.C = unwrap_params_for_C(partial_res1.x)
			res['nfev'] += partial_res1.nfev
			res['njev'] += partial_res1.njev

			#minimizing respect to v
			starting_point = self.v.flatten()
			partial_res2 = minimize(fun_optimize_for_v, starting_point, method='CG', jac=fun_grad_for_v, tol=tollerance)

			self.v = unwrap_params_for_v(partial_res2.x)
			res['nfev'] += partial_res2.nfev
			res['njev'] += partial_res2.njev

			norm_grad_C = np.transpose(fun_grad_for_C(partial_res1.x)).dot(fun_grad_for_C(partial_res1.x))
			norm_grad_v = np.transpose(fun_grad_for_v(partial_res2.x)).dot(fun_grad_for_v(partial_res2.x))

			res['iterations'] += 1
			if res['iterations']%2 == 0:
				tollerance = tollerance/10
			if self.verbose:
				print("grad for C: ", norm_grad_C)
				print("grad for v: ", norm_grad_v)

			stopping_criterion = ((norm_grad_C < epsilon_C) and (norm_grad_v < epsilon_v)) or (res['iterations'] > max_iteration)

		running_time = time.time() - start

		if self.verbose:
			print("###########################################")
			print("number of neurons used: ",self.neurons)
			print("value of sigma chosen: ",self.sigma)
			print("value of rho chosen: ",self.rho)
			print("norm of the gradient at the starting point: ",start_grad)
			print("value function at the starting point: ",start_fun_value)
			print("time spent to optimize the function: ",str(running_time))
			print("optimization routine used for centres: BFGS")
			print("optimization routine used for v: CG")
			print("number of function evaluation: ",res["nfev"])
			print("number of gradient evaluation: ",res["njev"])
			print("number of iterations: ",res["iterations"])
			print("norm of the grad at optimum: ",np.transpose(self.fun_grad(data)).dot(self.fun_grad(data)))
			print("value function at optimum: ",self.reg_error_function(data))
			print("###########################################")
