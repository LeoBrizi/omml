import sys
sys.path.append('../')
import pandas as pd
import numpy as np
from sklearn.model_selection import train_test_split
import argparse

from common.RBF import *


arg_parse = argparse.ArgumentParser(description='')
arg_parse.add_argument('--neurons', dest='neurons', type=int, default=47, help='# of neurons')
arg_parse.add_argument('--sigma', dest='sigma', type=float, default=0.9, help='value of sigma')
arg_parse.add_argument('--rho', dest='rho', type=float, default=1e-5, help='value of rho')
args = arg_parse.parse_args()

np.random.seed(1630230)

dataset_file = "../dataset/dataPoints.xlsx"

print("Loading the dataset...")
dataset = pd.read_excel(dataset_file, header=None).as_matrix()
# delete header of the excel file
dataset = np.delete(dataset, 0, 0)

print("dataset dimensions: "+str(dataset.shape[0]))

np.random.shuffle(dataset)
print("splitting the dataset in training e test set...")
train_set, test_set = train_test_split(dataset, test_size=0.15)

print("test set dimensions: " + str(test_set.shape))
print("train set dimension: " + str(train_set.shape))
# Newton-CG works well
# fix the hyper parameters after we find them with grid search
model = RBF(number_of_centres=args.neurons, input_dim=2,
            output_dim=1, sigma=args.sigma, rho=args.rho)
train_set = np.array(train_set, dtype=np.float64)
model.set_verbose(True)
model.decomposition_method(train_set)

model.plot_approximation()

print("*****************************************")
print("error value on train set: ",model.mse(train_set))
print("error value on test set: ",model.mse(test_set))
print("*****************************************")

